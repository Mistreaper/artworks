# Artworks

These are my very ugly artworks made by Krita. This repo is made for me to get feedback and improve on by bad art skills. Also, you may fork this repo and create a "merge request" if you would like your artwork to be showcased as well.

## Usage

Each artwork has a `.png` file and a `.kra` file. The `.png` files are the ones that you need. You need `krita` to open the `.kra` files. 

## Merge requests

They are like pull requests on Github.
You can create a merge request from your fork to contribute back to the main project.

1. On the top bar, select Menu > Project.

2. Select your fork of the repository.

3. On the left menu, go to Merge requests, and select New merge request.

4. In the Source branch drop-down list box, select the branch in your forked repository as the source branch.

5. In the Target branch drop-down list box, select the branch from the upstream repository as the target branch. You can set a default target project to change the default target branch (which can be useful if you are working in a forked project).

6. Select Compare branches and continue.

7. Select Submit merge request.


## Note

If you would like to have any of these artworks removed, create an issue on this gitlab page.